package com.afs.tdd;

public enum Command {
    TURN_LEFT, TURN_RIGHT, MOVE
}
