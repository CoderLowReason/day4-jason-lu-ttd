package com.afs.tdd;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import static com.afs.tdd.Direction.*;

public class MarsRover {
    private Location location;
    private final List<Direction> orderedDirections = Arrays.asList(NORTH, EAST, SOUTH, WEST);
    MarsRover executeCommand(Command command) {
        switch (command) {
            case MOVE:
                this.move(1);
                break;
            case TURN_LEFT:
                this.turnLeft();
                break;
            case TURN_RIGHT:
                this.turnRight();
                break;
        }
        return this;
    }

    private void turnRight() {
        int indexOfCurrentDirection = orderedDirections.indexOf(this.location.direction);
        int indexOfNextDirection = (indexOfCurrentDirection + 1) % orderedDirections.size();
        location.direction = orderedDirections.get(indexOfNextDirection);
    }

    private void turnLeft() {
        int indexOfCurrentDirection = orderedDirections.indexOf(this.location.direction);
        int indexOfNextDirection = (indexOfCurrentDirection - 1 + orderedDirections.size()) % orderedDirections.size();
        location.direction = orderedDirections.get(indexOfNextDirection);
    }

    private void move(int pace) {
        switch (location.direction) {
            case WEST:
                location.moveWest(pace);
                break;
            case EAST:
                location.moveEast(pace);
                break;
            case NORTH:
                location.moveNorth(pace);
                break;
            case SOUTH:
                location.moveSouth(pace);
                break;
        }
    }
    private static class Location {
        private int xCoordinate;
        private int yCoordinate;
        private Direction direction;

        private Location(int xCoordinate, int yCoordinate, Direction direction) {
            this.xCoordinate = xCoordinate;
            this.yCoordinate = yCoordinate;
            this.direction = direction;
        }

        private void moveNorth(int pace) {
            yCoordinate += pace;
        }

        private void moveWest(int pace) {
            xCoordinate -= pace;
        }

        private void moveSouth(int pace) {
            yCoordinate -= pace;
        }

        private void moveEast(int pace) {
            xCoordinate += pace;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Location location = (Location) o;
            return xCoordinate == location.xCoordinate && yCoordinate == location.yCoordinate && direction == location.direction;
        }

        @Override
        public int hashCode() {
            return Objects.hash(xCoordinate, yCoordinate, direction);
        }
    }
    public MarsRover(int xCoordinate, int yCoordinate, Direction direction) {
        location = new Location(xCoordinate, yCoordinate, direction);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MarsRover marsRover = (MarsRover) o;
        return Objects.equals(location, marsRover.location);
    }
    @Override
    public int hashCode() {
        return Objects.hash(location);
    }
}
