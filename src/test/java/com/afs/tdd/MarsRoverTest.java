package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MarsRoverTest {
    @Test
    void should_change_location_to_0_1_North_when_executeCommand_given_location_0_0_North_and_command_Move() {
        //given
        MarsRover rover = new MarsRover(0, 0, Direction.NORTH);
        //when
        rover.executeCommand(Command.MOVE);
        //then
        assertEquals(new MarsRover(0, 1, Direction.NORTH), rover);
    }
    @Test
    void should_change_location_to_0_0_East_when_executeCommand_given_location_0_0_North_and_command_turn_right() {
        //given
        MarsRover rover = new MarsRover(0, 0, Direction.NORTH);
        //when
        rover.executeCommand(Command.TURN_RIGHT);
        //then
        assertEquals(new MarsRover(0, 0, Direction.EAST), rover);
    }

    @Test
    void should_change_location_to_0_0_West_when_executeCommand_given_location_0_0_North_and_command_turn_left() {
        //given
        MarsRover rover = new MarsRover(0, 0, Direction.NORTH);
        //when
        rover.executeCommand(Command.TURN_LEFT);
        //then
        assertEquals(new MarsRover(0, 0, Direction.WEST), rover);
    }
    @Test
    void should_change_location_to_0_minus1_South_when_executeCommand_given_location_0_0_South_and_command_move() {
        //given
        MarsRover rover = new MarsRover(0, 0, Direction.SOUTH);
        //when
        rover.executeCommand(Command.MOVE);
        //then
        assertEquals(new MarsRover(0, -1, Direction.SOUTH), rover);
    }
    @Test
    void should_change_location_to_0_0_East_when_executeCommand_given_location_0_0_South_and_command_turn_left() {
        //given
        MarsRover rover = new MarsRover(0, 0, Direction.SOUTH);
        //when
        rover.executeCommand(Command.TURN_LEFT);
        //then
        assertEquals(new MarsRover(0, 0, Direction.EAST), rover);
    }

    @Test
    void should_change_location_to_0_0_West_when_executeCommand_given_location_0_0_South_and_command_turn_right() {
        //given
        MarsRover rover = new MarsRover(0, 0, Direction.SOUTH);
        //when
        rover.executeCommand(Command.TURN_RIGHT);
        //then
        assertEquals(new MarsRover(0, 0, Direction.WEST), rover);
    }

    @Test
    void should_change_location_to_1_0_East_when_executeCommand_given_location_0_0_East_and_command_move() {
        //given
        MarsRover rover = new MarsRover(0, 0, Direction.EAST);
        //when
        rover.executeCommand(Command.MOVE);
        //then
        assertEquals(new MarsRover(1, 0, Direction.EAST), rover);
    }

    @Test
    void should_change_location_to_0_0_North_when_executeCommand_given_location_0_0_East_and_command_turn_left() {
        //given
        MarsRover rover = new MarsRover(0, 0, Direction.EAST);
        //when
        rover.executeCommand(Command.TURN_LEFT);
        //then
        assertEquals(new MarsRover(0, 0, Direction.NORTH), rover);
    }
    @Test
    void should_change_location_to_0_0_South_when_executeCommand_given_location_0_0_East_and_command_turn_right() {
        //given
        MarsRover rover = new MarsRover(0, 0, Direction.EAST);
        //when
        rover.executeCommand(Command.TURN_RIGHT);
        //then
        assertEquals(new MarsRover(0, 0, Direction.SOUTH), rover);
    }
    @Test
    void should_change_location_to_minus1_0_West_when_executeCommand_given_location_0_0_West_and_command_move() {
        //given
        MarsRover rover = new MarsRover(0, 0, Direction.WEST);
        //when
        rover.executeCommand(Command.MOVE);
        //then
        assertEquals(new MarsRover(-1, 0, Direction.WEST), rover);
    }

    @Test
    void should_change_location_to_0_0_South_when_executeCommand_given_location_0_0_West_and_command_turn_left() {
        //given
        MarsRover rover = new MarsRover(0, 0, Direction.WEST);
        //when
        rover.executeCommand(Command.TURN_LEFT);
        //then
        assertEquals(new MarsRover(0, 0, Direction.SOUTH), rover);
    }
    @Test
    void should_change_location_to_0_0_North_when_executeCommand_given_location_0_0_West_and_command_turn_right() {
        //given
        MarsRover rover = new MarsRover(0, 0, Direction.WEST);
        //when
        rover.executeCommand(Command.TURN_RIGHT);
        //then
        assertEquals(new MarsRover(0, 0, Direction.NORTH), rover);
    }

    @Test
    void should_change_location_to_3_4_West_when_executeCommand_given_location_0_0_North_and_multiple_commands() {
        //given
        MarsRover rover = new MarsRover(0, 0, Direction.NORTH);
        //when
        rover.executeCommand(Command.TURN_RIGHT).executeCommand(Command.MOVE)
                .executeCommand(Command.MOVE)
                .executeCommand(Command.MOVE)
                .executeCommand(Command.TURN_LEFT)
                .executeCommand(Command.MOVE)
                .executeCommand(Command.MOVE)
                .executeCommand(Command.MOVE)
                .executeCommand(Command.MOVE)
                .executeCommand(Command.TURN_LEFT);
        //then
        assertEquals(new MarsRover(3, 4, Direction.WEST), rover);
    }
}
